import 'package:flutter/material.dart';
import 'package:profile_page/main.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        ));
  }

  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        ));
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBar(),
        body: biildBody(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                      ? currentTheme = APP_THEME.LIGHT
                      : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.textsms,
          //  color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionlButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Direction"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

//ListTile
Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-803-3390"),
    subtitle: Text("Mobile"),
    trailing: IconButton(
      icon: Icon(Icons.textsms),
      color: Colors.indigo.shade800,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.textsms),
      color: Colors.indigo.shade800,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("Care_Bear@gmail.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_pin),
    title: Text("403 California"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.indigo.shade800,
      onPressed: () {},
    ),
  );
}

AppBar buildAppBar() {
  return AppBar(
      backgroundColor: Colors.white,
      leading: Icon(
        Icons.arrow_back,
        color: Colors.black,
      ),
      actions: <Widget>[
        IconButton(
            onPressed: () {},
            icon: Icon(Icons.star_border),
            color: Colors.black),
      ]);
}

Widget biildBody() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            height: 300,
            color: Colors.amber[600],
            child: Image.network(
              "https://www.teahub.io/photos/full/81-814234_care-bears-laptop-background-pictures-old-care-bears.jpg",
              fit: BoxFit.cover,
            ),
          ),
          Container(
              height: 80,
              //color: Colors.amber[500],
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.all(20),
                    child: Text(
                      'Care Bears',
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                ],
              )),
          Divider(
            color: Colors.grey,
          ),
          Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: Theme(
                data: ThemeData(
                    iconTheme: IconThemeData(
                  color: Colors.purple,
                )),
                child: profileAction(),
              )),
          Divider(
            color: Colors.grey,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color: Colors.grey,
          ),
          emailListTile(),
          Divider(
            color: Colors.grey,
          ),
          addressListTile(),
        ],
      ),
    ],
  );
}

Widget profileAction() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoButton(),
      buildEmailButton(),
      buildDirectionlButton(),
      buildPayButton()
    ],
  );
}
